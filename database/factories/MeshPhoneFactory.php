<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\PbxStatus;
use App\Models\PbxType;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MeshPhone>
 */
class MeshPhoneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'           => '',
            'admin_name'     => '',
            'admin_email'    => '',
            'admin_phone'    => '',
            'admin_callsign' => '',
            'location'       => '',
            'latitude'       => '',
            'longitude'      => '',
            'hostname'       => '',
            'ip_address'     => '',
            'network'        => '',
            'pbx_status'     => PbxStatus::Unknown,
            'pbx_type'       => PbxType::Unknown,
            'office_codes'   => [],
            'trunks'         => [],
        ];
    }
}
