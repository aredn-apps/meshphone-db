<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\PbxStatus;
use App\Models\PbxType;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('network', function (Blueprint $table) {
            $table->string('pbx_status')->default(PbxStatus::Unknown->name);
            $table->string('pbx_type')->default(PbxType::Unknown->name);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('network', function (Blueprint $table) {
            $table->dropColumn('pbx_status');
        });
    }
};
