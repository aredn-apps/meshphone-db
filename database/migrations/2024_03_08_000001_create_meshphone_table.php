<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::connection('mongodb')->create('network', function (Blueprint $table) {
            $table->string('admin_name');
            $table->string('admin_phone');
            $table->string('admin_email');
            $table->string('admin_callsign');
            $table->string('name')->unique();
            $table->string('location');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('hostname')->unique();
            $table->string('ip_address')->unique();
            $table->string('pbx_type');
            $table->string('network')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('network');
    }
};
