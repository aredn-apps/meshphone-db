<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $grant_table = [
            'pbx_admin' => [
                'update-contact-info',
                'update-location',
                'edit-pbx',
            ],
            'exchange_admin' => [
                'create-pbx',
                'edit-pbx',
                'delete-pbx',
                'create-officecode',
                'edit-officecode',
                'delete-officecode',
                'create-dialplan',
                'edit-dialplan',
                'delete-dialplan',
                'create-trunk',
                'delete-trunk',
                'update-contact-info',
                'update-location',
            ],
            'db_admin' => [
                'create-pbx',
                'edit-pbx',
                'delete-pbx',
                'create-officecode',
                'edit-officecode',
                'delete-officecode',
                'create-dialplan',
                'edit-dialplan',
                'delete-dialplan',
                'create-trunk',
                'edit-trunk',
                'delete-trunk',
                'update-contact-info',
                'update-location',
                'manage-users',
                'manage-roles',
            ]
        ];

        // Create DB-admin
        foreach($grant_table as $role_name => $perms) {
            $role = Role::create(['name' => $role_name]);
            foreach($perms as $perm) {
                $role->givePermissionTo($perm);
            }
        }
    }
}
