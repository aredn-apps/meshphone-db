<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            'create-pbx',
            'edit-pbx',
            'delete-pbx',
            'create-officecode',
            'edit-officecode',
            'delete-officecode',
            'create-dialplan',
            'edit-dialplan',
            'delete-dialplan',
            'create-trunk',
            'edit-trunk',
            'delete-trunk',
            'update-contact-info',
            'update-location',
            'manage-users',
            'manage-roles',
         ];

         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
