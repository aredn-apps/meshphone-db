<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::create([
            'name' => 'Gerard Hickey',
            'email' => 'hickey@kinetic-compute.com',
            'password' => bcrypt('test1234')
        ]);

        $role = Role::create(['name' => 'Admin']);

        $permissions = Permission::pluck('name','name')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->name]);
    }
}
