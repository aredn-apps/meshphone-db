<?php

namespace App\Helpers;

use App\Models\MeshPhone;
use App\Models\PbxStatus;
use App\Models\PbxPermission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class Helper {
    public static function locateOfficeCode(string $office_code): ?MeshPhone {
        $pbx = MeshPhone::firstWhere('office_codes.office_code', $office_code);
        return $pbx;
    }

    public static function firstOfficeCode(string $pbx_name): string {
        return Helper::pbxInfo($pbx_name)->office_codes[0]['office_code'];
    }

    public static function pbxNameList(): Array {
        $pbx = MeshPhone::whereIn('pbx_status', [
            PbxStatus::Active->name,
            PbxStatus::Inactive->name,
            PbxStatus::Provisioning->name,
        ])->get()->pluck('name');

        return $pbx->toArray();
    }

    public static function pbxInfo(string $pbx_name): ?MeshPhone {
        $pbx = MeshPhone::firstWhere('name', $pbx_name);
        return $pbx;
    }

    public static function DateColor($when): string {
        function bg(string $color) { return "background-color: $color; "; }
        function fg(string $color) { return "color: $color; "; }

        if ($when == NULL) {
            return bg('dimgrey') . fg('linen');
        }

        $diff = $when->diffInHours(now());
        if ($diff < 1) {
            return bg('green') . fg('linen');
        } elseif ($diff < 8) {
            return bg('lightgreen') . fg('charcoal');
            // return bg('yellow') . fg('charcoal');
            // return bg('#FFBF00') . fg('charcoal');
            // return bg('#C21E56') . fg('linen');
        } elseif ($diff < 24) {
            return bg('yellow') . fg('charcoal');
        } elseif ($diff < 72) {
            return bg('#FFBF00') . fg('charcoal');
        } else {
            return bg('#C21E56') . fg('linen');
        }
    }

    public static function resolveTrunks(Request $request, array &$pbx) {
        // first we need a snapshot of existing trunks to detect deleted trunks
        $pbx_info = MeshPhone::find($request->_id);
        $user = $request->user();
        $existing_trunks = [];
        if ($pbx_info && $pbx_info->trunks) {
            $existing_trunks = Arr::map($pbx_info->trunks,
                function(array $trunk, int $key){
                    if (Arr::exists($trunk, 'pbx_name')) {
                        return $trunk['pbx_name'];
                    } else {
                        return Helper::locateOfficeCode($trunk['office_code'])->name;
                    }
                });
        }

        $proposed_trunks = [];
        if (array_key_exists('trunks', $pbx)) {
            $proposed_trunks = Arr::map($pbx['trunks'],
            function(array $trunk, int $key) {
                return $trunk['pbx_name'];
            });
        }


        // look for deleted trunks and new trunks
        $deleted_trunks = array_diff($existing_trunks, $proposed_trunks);
        $new_trunks = array_diff($proposed_trunks, $existing_trunks);

        // update the remote PBXes with trunk removal
        if ($user->hasPermissionTo(PbxPermission::DeleteTrunk)) {
            foreach($deleted_trunks as $name) {
                Helper::removeTrunkFromPbx($pbx['name'], $name);
            }
        }

        // update the remote PBXes with new trunk
        if ($user->hasPermissionTo(PbxPermission::CreateTrunk)) {
            foreach($new_trunks as $name) {
                Helper::addTrunkToPbx($pbx['name'], $name);
            }
        }
    }

    public static function addTrunkToPbx(string $origin, string $pbx_name) {
        // info("addTrunkToPbx($origin, $pbx_name)");
        $pbx = MeshPhone::firstWhere('name', $pbx_name)->toArray();
        $pbx['trunks'][] = array(
            'pbx_name' => $origin,
            'office_code' => Helper::firstOfficeCode($origin),
        );

        $record = MeshPhone::find($pbx['_id']);
        unset($pbx['_id']);
        unset($pbx['created_at']);
        unset($pbx['updated_at']);
        unset($pbx['checkin_at']);
        $record->update($pbx);
    }

    public static function removeTrunkFromPbx(string $origin, string $pbx_name) {
        // info("removeTrunkFromPbx($origin, $pbx_name)");
        $pbx = MeshPhone::firstWhere('name', $pbx_name);
        $pbx->trunks = Arr::where($pbx->trunks,
            function(array $trunk, int $key) use ($origin) {
                return $trunk['pbx_name'] != $origin;
        });
        $pbx->save();
    }

    public static function validateDialplan(Request $req): Array {
        $office_codes = $req->get('office_codes');
        $errors = [];
        $count = 0;
        foreach($office_codes as $office) {
            foreach($office['dialplans'] as $plan) {
                if (substr_count($plan, 'x') > 2) {
                    $errors["dialplans_$count"] = "A dailplan exceeds acceptble use of wildcards";
                }
            }
            $count = $count + 1;
        }

        return $errors;
    }
}
