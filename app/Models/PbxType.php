<?php

namespace App\Models;

enum PbxType: string {
    case Unknown = 'Unknown';
    case Allstar = 'Allstar';
    case Asterisk = 'Asterisk';
    case FreePBX = 'FreePBX';
    case GrandStream = 'GrandStream';
    case IncrediblePBX = 'IncrediblePBX';
    case MeshPBX = 'MeshPBX';
    case RasPBX = 'RasPBX';
}
