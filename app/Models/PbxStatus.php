<?php

namespace App\Models;

enum PbxStatus: string {
    case Unknown = 'Unknown';
    case Application = 'Application';
    case Provisioning = 'Provisioning';
    case Active = 'Active';
    case Maintenance = 'Maintenance';
    case Inactive = 'Inactive';
    case Decommission = 'Decommission';
}
