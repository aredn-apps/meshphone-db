<?php

namespace App\Models;

use MongoDB\Laravel\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MeshPhone extends Model
{
    use HasFactory;

    const MAX_LATENCY_HISTORY = 12;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "network";

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = '_id';
    // protected $keyType = 'string';

    /**
     * Get the columns that should receive a unique identifier.
     *
     * @return array<int, string>
     */
    public function uniqueIds(): array
    {
        return ['_id'];
    }

    /**
     * The database connection that should be used by the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'trunks' => [],
        'office_codes' => [],
        'pbx_status' => PbxStatus::Unknown->name,
        'pbx_type' => PbxType::Unknown->name,
    ];

    protected $dates = ['created_at', 'updated_at', 'checkin_at'];
    protected $casts = [
        'checkin_at' => 'datetime',
    ];
    protected $dateFormat = "Y-m-d  H:i:s";

    protected $collection = "network";
    protected $fillable = [
        'name',
        'admin_callsign',
        'admin_name',
        'admin_phone',
        'admin_email',
        'hostname',
        'ip_address',
        'latitude',
        'longitude',
        'location',
        'network',
        'pbx_type',
        'office_codes',
        'trunks',
        'pbx_status',
        'gen_script_version',
        'gen_script_name',
    ];

    public function updateTrunkLatency(string $pbx_name, int $latency) {
        //info("MeshPhone::updateTrunkLatency($pbx_name, $latency)");
        $trunks = $this->trunks;
        foreach($trunks as $key=>$entry) {
            if ($entry['pbx_name'] == $pbx_name) {
                // Add $latency to the front of latency_history and
                // truncate array to MAX_LATENCY_HISTORY
                $trunks[$key]['latency_history'] = array_merge([ $latency ],
                    array_slice($trunks[$key]['latency_history'] ?? [], 0,
                        MeshPhone::MAX_LATENCY_HISTORY-1));
                // Recalculate the average latency and store it
                $trunks[$key]['latency'] = (int)(array_sum($trunks[$key]['latency_history']) / count($trunks[$key]['latency_history']));
            }
        }
        $this->trunks = $trunks;
    }

}



