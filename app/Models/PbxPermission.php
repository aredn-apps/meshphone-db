<?php

namespace App\Models;

class PbxPermission {
    const CreatePBX         = 'create-pbx';
    const EditPBX           = 'edit-pbx';
    const DeletePBX         = 'delete-pbx';
    const CreateOfficecode  = 'create-officecode';
    const EditOfficecode    = 'edit-officecode';
    const DeleteOfficecode  = 'delete-officecode';
    const CreateDialplan    = 'create-dialplan';
    const EditDialplan      = 'edit-dialplan';
    const DeleteDialplan    = 'delete-dialplan';
    const CreateTrunk       = 'create-trunk';
    const EditTrunk         = 'edit-trunk';
    const DeleteTrunk       = 'delete-trunk';
    const UpdateContactInfo = 'update-contact-info';
    const UpdateLocation    = 'update-location';
    const ManageUsers       = 'manage-users';
    const ManageRoles       = 'manage-roles';

}


