<?php

namespace App\Http\Controllers;

use App\Models\MeshPhone;
use App\Models\PbxType;
use App\Models\PbxStatus;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): View
    {
        $pbxes = MeshPhone::all();

        // sumarize PBX status
        $status_count = array();
        $total = 0;
        foreach(Arr::pluck($pbxes, 'pbx_status', 'name') as $name=>$status) {
            $status_count[$status] = Arr::exists($status_count, $status) ?
                $status_count[$status] + 1 : 1;
                $total++;
        }

        return view('dashboard', compact('total', 'status_count'));
    }
}
