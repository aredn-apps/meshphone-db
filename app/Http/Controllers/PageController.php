<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class PageController extends Controller
{
    public function show($id): View
    {
        $filename = $id;
        if (!file_exists($filename)) {
            $filename .= '.md';
            if (!file_exists($filename)) {
                return abort(404);
            }
        }
        $content = file_get_contents($filename);
        $title = ucwords(str_replace('_', ' ', str_replace('.md', '', $filename)));
        return view('markdown', compact('content', 'title'));
    }
}
