<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MeshPhone;
use Illuminate\Support\Facades\Log;

class ExportController extends Controller
{

    public function network_json()
    {
        return MeshPhone::all();
    }

    public function network_xml_v2()
    {
        $xml = simplexml_load_string('<?xml version="1.0" encoding="ISO-8859-1"?><network/>');
        foreach(MeshPhone::all()->toArray() as $pbx) {
            $pbx_def = $xml->addChild('pbx');

            $admin = $pbx_def->addChild('admin');
            $admin->addChild('name', $pbx['admin_name']);
            $admin->addChild('callsign', $pbx['admin_callsign']);
            $admin->addChild('phone', $pbx['admin_phone']);
            $admin->addChild('email', $pbx['admin_email']);

            $loc = $pbx_def->addChild('location', $pbx['location']);
            $coord = $loc->addChild('coordinates');
            $coord->addAttribute('latitude', $pbx['latitude']);
            $coord->addAttribute('longitude', $pbx['longitude']);

            $pbx_def->addAttribute('name', $pbx['name']);
            $pbx_def->addAttribute('type', $pbx['pbx_type']);
            $pbx_def->addChild('hostname', $pbx['hostname']);
            $pbx_def->addChild('ip_address', $pbx['ip_address']);

            $offices = $pbx_def->addChild('office_codes');
            foreach($pbx['office_codes'] as $entry) {
                $office = $offices->addChild('office');
                $office->addAttribute('code', $entry['office_code']);
                $office->addAttribute('npa', $entry['npa']);
                $dialplans = $office->addChild('dialplans');
                if (array_key_exists('dialplans', $entry)) {
                    foreach($entry['dialplans'] as $rule) {
                        $dialplans->addChild('dialplan')
                            ->addAttribute('pattern', $rule);
                    }
                }
            }

            $trunks = $pbx_def->addChild('trunks');
            foreach($pbx['trunks'] as $value) {
                $trunk = $trunks->addChild('trunk');
                $trunk->addAttribute('office_code', $value['office_code']);
                $trunk->addAttribute('pbx_name', $value['pbx_name']);
            }
        }

        return $xml->asXML();
    }

    public function network_xml_v1()
    {
        // Separate PBX record into pbx, dialplan and trunk arrays
        $pbx = array();
        $dialplan = array();
        $trunks = array();
        foreach(MeshPhone::all()->toArray() as $entry) {
            $last_update = $entry['created_at'];
            if (array_key_exists('updated_at', $entry)) {
                $last_update = $entry['updated_at'];
            }

            $pbx[] = array(
                'contact_email' => $entry['admin_email'],
                'contact_phone' => $entry['admin_phone'],
                'owner_callsign' => $entry['admin_callsign'],
                'location' => $entry['location'],
                'lat' => $entry['latitude'],
                'lon' => $entry['longitude'],
                'npa' => $entry['office_codes'][0]['npa'],
                'office_code' => $entry['office_codes'][0]['office_code'],
                'serving_node' => $entry['hostname'],
                'ip_address' => $entry['ip_address'],
                'type' => $entry['pbx_type'],
                'network' => $entry['network'],
                'last_upd' => $last_update,
            );

            foreach($entry['office_codes'] as $office) {
                if (array_key_exists('dialplans', $office)) {
                    foreach($office['dialplans'] as $rule) {
                        $dialplan[] = array(
                            'translation' => $rule,
                            'serving_office' => $office['office_code'],
                            'last_upd' => $last_update,
                        );
                    }
                }
            }

            foreach($entry['trunks'] as $trunk) {
                $trunks[] = array(
                    'office_code_a' => $entry['office_codes'][0]['office_code'],
                    'pbx_name_a' => $entry['name'],
                    'office_code_z' => $trunk['office_code'],
                    'pbx_name_z' => $trunk['pbx_name'],
                    'network' => $entry['network'],
                    'last_upd' => $last_update,
                );
            }
        }

        $xml = simplexml_load_string('<?xml version="1.0" encoding="ISO-8859-1"?><meshphone/>');

        // Add the PBXes
        foreach($pbx as $entry) {
            $child = $xml->addChild('pbx');
            ExportController::array_to_xml($entry, $child);
        }

        // Add the dialplans
        foreach($dialplan as $plan) {
            $child = $xml->addChild('dialplan');
            ExportController::array_to_xml($plan, $child);
        }

        // Add the trunks
        foreach($trunks as $trunk) {
            $child = $xml->addChild('trunks');
            ExportController::array_to_xml($trunk, $child);
        }

        return $xml->asXML();
    }

    private function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $key = 'item'.$key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                ExportController::array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
    }
}

