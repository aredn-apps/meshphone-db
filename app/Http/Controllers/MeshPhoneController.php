<?php

namespace App\Http\Controllers;

use App\Models\MeshPhone;
use App\Models\PbxType;
use App\Models\PbxStatus;
use App\Models\PbxPermission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use MongoDB\Laravel\Eloquent\Model;
use App\Http\Requests\MeshPhoneRequest;
use App\Helpers\Helper;
//use Illuminate\Database\Eloquent\Collection;

class MeshPhoneController extends Controller
{
    const VALIDATIONS = array(
        'name' => 'required|string|uppercase|max:16|not_regex:/\s/|not_regex:/^\d+$/',
        'admin_name' => 'required|string',
        'admin_email' => 'required|string|email',
        'admin_phone' => 'required|string|regex:/^\d{3}-\d{4}$/',
        'admin_callsign' => 'required|string|uppercase|min:3',
        'location' => 'required|string',
        'latitude' => 'required|numeric',
        'longitude' => 'required|numeric',
        'hostname' => 'nullable|string|max:64',
        'ip_address' => 'nullable|ip|string',
        'network' => 'nullable',
        'pbx_type' => 'required|string',
        'office_codes' => 'array',
        'trunks' => 'array',
        'pbx_status' => 'required|string',
    );

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        // $pbx = MeshPhone::where('location', '=', 'DeLand, FL')->get();
        return view('meshphone.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        info('MeshPhoneController.create()');

        $pbx = MeshPhone::factory()->make();
        $pbx->pbx_status = PbxStatus::Provisioning->name;

        return view('meshphone.create', [
            'pbx' => $pbx,
            'edit' => 1,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MeshPhoneRequest $request): RedirectResponse
    {
        info('MeshPhoneController.store($request)');

        // just make sure ......
        $user = $request->user();
        if (!$user->hasPermissionTo(PbxPermission::CreatePBX)) {
            info("ERROR: {$user->callsign} attempted to create a PBX but does not have permission");
            return to_route('meshphone.index')->with('error', 'No permission to create PBX');
        }

        // Process any validation that needs to be done
        $validated = $request->validate(MeshPhoneController::VALIDATIONS);
        $errors = Helper::validateDialplan($request);
        if ($errors) {
            return redirect()->back()->withInput()->withErrors($errors);
        }

        Helper::resolveTrunks($request, $validated);

        // add office code for trunks being added
        if (array_key_exists('trunks', $validated)) {
            foreach($validated['trunks'] as &$trunk) {
                if (Arr::exists($trunk, 'office_code') == false) {
                    $trunk['office_code'] = Helper::firstOfficeCode($trunk['pbx_name']);
                }
            }
        }

        MeshPhone::create($validated);

        return to_route('meshphone.index')->with('success', 'PBX record stored successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show($id): View
    {
        info("MeshPhoneController.show($id)");

        return view('meshphone.show', [
            'pbx' => MeshPhone::where('_id', '=', $id)->first(),
            'edit' => 0,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id): View
    {
        info("MeshPhoneController::edit({{$id}})");

        return view('meshphone.edit', [
            'pbx' => MeshPhone::where('_id', '=', $id)->get()->first(),
            'edit' => 1,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MeshPhoneRequest $request, MeshPhone $meshPhone)
    {
        info('MeshPhoneController.update($request)');
        $user = $request->user();

        // Is the request flagged to be deleted?
        if ($request->delete == "delete") {
            info('Request indicates that record should be deleted');
            return to_route('meshphone.delete', $request->_id);
        }

        // Process any validation that needs to be done
        $needed_validations = Arr::only(MeshPhoneController::VALIDATIONS, array_keys($request->all()));
        $validated = $request->validate($needed_validations);
        $errors = Helper::validateDialplan($request);
        if ($errors) {
            return redirect()->back()->withInput()->withErrors($errors);
        }

        if ($user->hasAnyPermission([PbxPermission::CreateTrunk,
                                     PbxPermission::EditTrunk,
                                     PbxPermission::DeleteTrunk])) {
            Helper::resolveTrunks($request, $validated);

            if (array_key_exists('trunks', $validated)) {
                // add office code for trunks being added
                foreach($validated['trunks'] as &$trunk) {
                    if (Arr::exists($trunk, 'office_code') == false) {
                        $trunk['office_code'] = Helper::firstOfficeCode($trunk['pbx_name']);
                    }
                }
            } elseif ($user->hasPermissionTo(PbxPermission::DeleteTrunk)) {
                // trunks were validated, but nothing exists, create empty array
                $validated['trunks'] = array();
            }
        }

        $record = MeshPhone::find($request->_id);
        $record->update($validated);

        if ($user->hasPermissionTo(PbxPermission::EditPBX)) {
            $old_pbx_name = $request->input('orig_name');
            if ($validated['name'] != $old_pbx_name) {
                // PBX name is being changed. Update all trunks.
                foreach($validated['trunks'] as $trunk) {
                    Helper::removeTrunkFromPbx($old_pbx_name, $trunk['pbx_name']);
                    Helper::addTrunkToPbx($validated['name'], $trunk['pbx_name']);
                }
            }
        }

        return to_route('meshphone.index')->with('success', 'PBX record updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        info("MeshPhoneController::destroy($id)");

        if (!$user->hasPermissionTo(PbxPermission::DeletePBX)) {
            info("ERROR: {$user->callsign} attempted to delete a PBX but does not have permission");
            return to_route('meshphone.index')->with('error', 'No permission to delete PBX');
        }

        $record = MeshPhone::where('_id', '=', $id);
        $record->delete();

        return to_route("meshphone.index")->with('success', 'PBX record deleted');
    }

    // This function is required to trigger a record to be deleted from the
    // edit screen.
    public function delete($id)
    {
        info("MeshPhoneController::delete($id)");
        return route("meshphone.destroy", $id);
    }

    public function search(Request $req)
    {
        info('MeshPhoneController->search($request)');
        $term = $req['search_for'];

        $results = MeshPhone::whereAny([
            'name', 'admin_name', 'admin_email', 'admin_phone',
            'admin_callsign', 'location', 'network', 'hostname',
            'ip_address'], 'LIKE', "%$term%")
            ->orWhere('office_codes.office_code', '=', $term)
            ->orderBy('name', 'asc')->get();

        return view("meshphone.index")->with('results', $results);
    }
}
