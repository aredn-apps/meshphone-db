<?php

namespace App\Http\Controllers;

use App\Models\MeshPhone;
use App\Models\PbxType;
use App\Models\PbxStatus;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CheckinController extends Controller
{
    const VALIDATIONS = array(
        'pbx_name' => 'required|string|uppercase|max:16|not_regex:/\s/',
        'hostname' => 'nullable|string|max:64',
        'ip_address' => 'nullable|ip|string',
        'pbx_type' => 'required|string',
        'office_codes' => 'array',
        'trunks' => 'array',
        'pbx_status' => 'required|string',
        'gen_script_name' => 'string',
        'gen_script_version' => 'string',
    );

    /**
     * PBX checkin during dialplan generation
     */
    public function checkin(Request $request)
    {
        $needed_validations = Arr::only(CheckinController::VALIDATIONS, array_keys($request->all()));
        $validated = $request->validate($needed_validations);

        try{
            $pbx_record = MeshPhone::where('name', $validated['pbx_name'])->first();
        }
        catch (ErrorException $e) {
            Log::error("Checkin from PBX with invalid checkin data");
            Log::error(var_export($validated, true));
            return;
        }

        if (!$pbx_record) {
            Log::error("Invalid checkin: no PBX name provided.");
            Log::error(var_export($validated, true));
            return abort(404);
        }

        Log::info("CheckinController::checkin(" . $pbx_record->name . ")");

        // update fields that may have been supplied
        if (Arr::exists($validated, 'hostname')) {
            $pbx_record->hostname = $validated['hostname'];
        }
        if (Arr::exists($validated, 'ip_address')) {
            $pbx_record->ip_address = $validated['ip_address'];
        }
        if (Arr::exists($validated, 'pbx_type')) {
            $pbx_record->pbx_type = $validated['pbx_type'];
        }
        if (Arr::exists($validated, 'gen_script_name')) {
            $pbx_record->gen_script_name = $validated['gen_script_name'];
        }
        if (Arr::exists($validated, 'gen_script_version')) {
            $pbx_record->gen_script_version = $validated['gen_script_version'];
        }

        // process any trunk information being supplied
        if (Arr::exists($validated, 'trunks')) {
            foreach($validated['trunks'] as $entry) {
                if ($entry['latency'] == 'UNREACHABLE' or $entry['latency'] == 'Unreachable') {
                    $pbx_record->updateTrunkLatency($entry['pbx_name'], 9999);
                } else {
                    $pbx_record->updateTrunkLatency($entry['pbx_name'], $entry['latency']);
                }
            }
        }

        // if the PBX is in maintenance status, this check in indicates
        // that it is operational again and can be moved back to active
        if ($pbx_record->pbx_status == PbxStatus::Maintenance->name) {
            $pbx_record->pbx_status = PbxStatus::Active;
        }

        $pbx_record->checkin_at = now();
        $pbx_record->save();
    }


}
