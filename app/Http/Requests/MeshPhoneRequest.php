<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeshPhoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
        ];
    }

    public function prepareForValidation()
    {
        if ($this->admin_callsign) {
            $this->merge(['admin_callsign' => strtoupper($this->admin_callsign)]);
        }

        if ($this->name) {
            $this->merge(['name' => strtoupper($this->name)]);
        }
    }
}
