

## [1.13.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.12.3...v1.13.0) (2024-06-19)


### Features

* Support updating trunk when renaming a PBX ([57a0265](https://gitlab.com/aredn-apps/meshphone-db/commit/57a0265cc7706792576173c247760a5ca73d942f))


### Bug Fixes

* Remove vite for d3.min.js ([7c253dc](https://gitlab.com/aredn-apps/meshphone-db/commit/7c253dcc5b4a06ae567414c7747500eff8d6f69f))

## [1.12.3](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.12.2...v1.12.3) (2024-06-11)


### Bug Fixes

* Refine how script name and version are displayed ([848c7a1](https://gitlab.com/aredn-apps/meshphone-db/commit/848c7a1246644504562677ad1a2b31691e618579))

## [1.12.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.12.1...v1.12.2) (2024-06-11)


### Bug Fixes

* Add support for gen_script_name/version ([629b1c5](https://gitlab.com/aredn-apps/meshphone-db/commit/629b1c5b50a026c4850cd3fdee6ccea89cc3c9ec))

## [1.12.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.12.0...v1.12.1) (2024-05-29)


### Bug Fixes

* Handle unreachable trunks properly ([b4c4420](https://gitlab.com/aredn-apps/meshphone-db/commit/b4c4420172a404daab52487d2b40b5073fac024c))

## [1.12.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.11.0...v1.12.0) (2024-05-19)


### Features

* More icons indicating status ([7a997e9](https://gitlab.com/aredn-apps/meshphone-db/commit/7a997e96c95a33371260f421c961015ca43f8847))

## [1.11.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.8...v1.11.0) (2024-05-19)


### Features

* Add basic dashboard ([40ddbdf](https://gitlab.com/aredn-apps/meshphone-db/commit/40ddbdf951480f58c136dd28752c30e15c864e96))

## [1.10.8](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.7...v1.10.8) (2024-05-17)


### Bug Fixes

* Checking for acceptable dialplan wildcards ([ce15774](https://gitlab.com/aredn-apps/meshphone-db/commit/ce157749ade40869fb14e072aa987b1d02c3848f))

## [1.10.7](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.6...v1.10.7) (2024-05-11)

## [1.10.6](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.5...v1.10.6) (2024-05-06)


### Bug Fixes

* Fix lists and preformatted text on markdown pages ([f78cd9c](https://gitlab.com/aredn-apps/meshphone-db/commit/f78cd9cc49e3cf74491099f726ddf9f0ba207f18))

## [1.10.5](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.4...v1.10.5) (2024-04-27)


### Bug Fixes

* Add protection when frontpage.md is missing ([af494ea](https://gitlab.com/aredn-apps/meshphone-db/commit/af494ea3ac4a24edf0fcd135ef9fb5125f0426d6))
* Log invalid checkin calls ([27dc64b](https://gitlab.com/aredn-apps/meshphone-db/commit/27dc64bccecb4c12b94b37b1f45d4720f5386bb1))

## [1.10.4](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.3...v1.10.4) (2024-04-25)


### Bug Fixes

* Add logging to catch certain errors ([55f8f35](https://gitlab.com/aredn-apps/meshphone-db/commit/55f8f356d0d4f3ee02fff8a52457d94bf1b19432))

## [1.10.3](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.2...v1.10.3) (2024-04-23)


### Bug Fixes

* Remove errant parethesis ([66372d6](https://gitlab.com/aredn-apps/meshphone-db/commit/66372d6624721106115100e216fbf5ad89fa9b1b))

## [1.10.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.1...v1.10.2) (2024-04-23)


### Bug Fixes

* User role assignment ([69da87d](https://gitlab.com/aredn-apps/meshphone-db/commit/69da87d71dae42725524f99109f69a5b846083ff))

## [1.10.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.10.0...v1.10.1) (2024-04-20)


### Bug Fixes

* Minor UI improvements ([8db49d9](https://gitlab.com/aredn-apps/meshphone-db/commit/8db49d989992b154b924eb7469c1e8543c0d18bc))

## [1.10.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.9.2...v1.10.0) (2024-04-20)


### Features

* Add API to handle PBX checkins ([72ec715](https://gitlab.com/aredn-apps/meshphone-db/commit/72ec715bf405d5000540a5af5456072e35c66f96))

## [1.9.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.9.1...v1.9.2) (2024-04-15)


### Bug Fixes

* Remove frontpage.md so it can be maintained seperately ([6812b32](https://gitlab.com/aredn-apps/meshphone-db/commit/6812b32ae515845b96255368ba4923b7b206a46d))

## [1.9.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.9.0...v1.9.1) (2024-04-15)


### Bug Fixes

* Add navigation template to markdown pages ([b2fab30](https://gitlab.com/aredn-apps/meshphone-db/commit/b2fab302eca416384567406adc9b7fc8d1e2a516))
* Correctly display release version in heading ([22a7928](https://gitlab.com/aredn-apps/meshphone-db/commit/22a792875c1c04a66d2dc4cc6082e4db2496616b))

## [1.9.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.8.0...v1.9.0) (2024-04-14)


### Features

* Add PageController to display static pages ([2e6bd50](https://gitlab.com/aredn-apps/meshphone-db/commit/2e6bd50c293dababebd4f7e89aba74f4c454db45))


### Bug Fixes

* Add Applicaiton state to PBX status ([5f6de15](https://gitlab.com/aredn-apps/meshphone-db/commit/5f6de15f5b4da5be1968e76b4873bed9edb27bd5))
* Handle when trunk information is incomplete ([cf73d41](https://gitlab.com/aredn-apps/meshphone-db/commit/cf73d41d93e99a7793e576f30a327f13ebb0e5a2))
* Use enums in MeshPhone factory ([4991b89](https://gitlab.com/aredn-apps/meshphone-db/commit/4991b89e8d36a9462a602e5b62bb110606cffeba))

## [1.8.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.7.0...v1.8.0) (2024-04-09)


### Features

* Update displayed version number ([286dc19](https://gitlab.com/aredn-apps/meshphone-db/commit/286dc19901b6a696a5198d316d8aeeb6b190011f))


### Bug Fixes

* Fix PBX admins updating their PBXes ([f222b4b](https://gitlab.com/aredn-apps/meshphone-db/commit/f222b4bc050de26f423770d718b3b79b7dd2ffc0))

## [1.7.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.6.2...v1.7.0) (2024-04-03)


### Features

* User management update to make useable ([db470bf](https://gitlab.com/aredn-apps/meshphone-db/commit/db470bf5a3a10b655a90c13e90fc51630ac659fc))


### Bug Fixes

* Allow registrations to be processed ([0a8ada6](https://gitlab.com/aredn-apps/meshphone-db/commit/0a8ada60140eeb8c6b8b620975a0b36cf901059a))
* Handle updates when no trunks specified ([65ef3c2](https://gitlab.com/aredn-apps/meshphone-db/commit/65ef3c282382a874c02cc7e9d54aa37e610d966f))
* Redirect exports to /api endpoint ([3d377d4](https://gitlab.com/aredn-apps/meshphone-db/commit/3d377d436daa5d6580e1d978e83003ce3da770a9))

## [1.6.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.6.1...v1.6.2) (2024-04-02)


### Bug Fixes

* Handle updates when no trunks listed ([e809915](https://gitlab.com/aredn-apps/meshphone-db/commit/e809915cf3104226bdcd2547fe36ff5b89223b61))

## [1.6.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.6.0...v1.6.1) (2024-03-28)


### Bug Fixes

* Handle when no trunks are specified ([672b61c](https://gitlab.com/aredn-apps/meshphone-db/commit/672b61c483aebd09d060bad3167a5e4e898f8382))

## [1.6.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.5.0...v1.6.0) (2024-03-28)


### Features

* Add PBX names in trunks for exported formats ([b19d990](https://gitlab.com/aredn-apps/meshphone-db/commit/b19d9900279d1ca6bac87449d6046852bcfae1e6))
* Add support for markdown welcome page ([32d8e42](https://gitlab.com/aredn-apps/meshphone-db/commit/32d8e423e42347d0cc5c77c58aa239dc5f332dca))

## [1.5.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.4.1...v1.5.0) (2024-03-28)


### Features

* Add PBX name column to trunk table ([cab55cf](https://gitlab.com/aredn-apps/meshphone-db/commit/cab55cf5cb30c5995e8c75835c327b9119ddb435))
* Add PBX name to trunk table ([bd1ff14](https://gitlab.com/aredn-apps/meshphone-db/commit/bd1ff1403a8a579029402e6b2d95e0e983e77e19))
* Switch trunk entry to using PBX names ([c6cb2dd](https://gitlab.com/aredn-apps/meshphone-db/commit/c6cb2dd4896fe45ebc33b832bcfa3dd96cc7fed6))


### Bug Fixes

* Adding new PBX sets status to Provisioning ([d442e9c](https://gitlab.com/aredn-apps/meshphone-db/commit/d442e9c084bf4e5681374ba1ec4d1016d309d69f))

## [1.4.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.4.0...v1.4.1) (2024-03-25)

## [1.4.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.3.3...v1.4.0) (2024-03-25)


### Features

* Add PBX status to search results ([5789f07](https://gitlab.com/aredn-apps/meshphone-db/commit/5789f07483adcf59acc26b194a4ce6b6c4a257a8))

## [1.3.3](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.3.2...v1.3.3) (2024-03-25)


### Bug Fixes

* Restrict add pbx to create-pbx permission ([ce9daf1](https://gitlab.com/aredn-apps/meshphone-db/commit/ce9daf1d605b2aee578c251ca4e5a5237d42ee5d))

## [1.3.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.3.1...v1.3.2) (2024-03-25)


### Bug Fixes

* Customize validations to received fields ([8210b23](https://gitlab.com/aredn-apps/meshphone-db/commit/8210b23af044a9762027190abdd15d29f7bfb74f))

## [1.3.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.3.0...v1.3.1) (2024-03-25)


### Bug Fixes

* Disable fields not editable by user ([4a48164](https://gitlab.com/aredn-apps/meshphone-db/commit/4a48164ec5c81426a16e991445d784f1c5a3effd))

## [1.3.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.2.0...v1.3.0) (2024-03-25)


### Features

* Add callsign to registration and user DB ([da93ec3](https://gitlab.com/aredn-apps/meshphone-db/commit/da93ec3cd0e34ef4406efc84be4ef94f574c084c))
* Add user access levels to meshphone edit page ([d3fbfad](https://gitlab.com/aredn-apps/meshphone-db/commit/d3fbfad85f75d7765cdcc5cf5b7af684fd884811))

## [1.2.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.1.0...v1.2.0) (2024-03-25)


### Features

* Add delete PBX functionality ([d903773](https://gitlab.com/aredn-apps/meshphone-db/commit/d903773f9dbf83ebd45ffce22c09a6352e8fa680))
* Add PBX status field ([299272d](https://gitlab.com/aredn-apps/meshphone-db/commit/299272d47fe6dc20d62c77ded085aa3e07fd3dd3))
* Generate JSON and XML of database ([2464439](https://gitlab.com/aredn-apps/meshphone-db/commit/24644391914282efe6b5742db55fe629aa2ac65d))
* Search enabled for office codes ([76fe3ae](https://gitlab.com/aredn-apps/meshphone-db/commit/76fe3aeb7e247bf18d3a204f6f2cf6ee4912ae45))


### Bug Fixes

* Fix records being deleted when edited ([41ed33b](https://gitlab.com/aredn-apps/meshphone-db/commit/41ed33b41abb4c3b1742beaafaca7b954946bb45))

## [1.1.0](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.0.4...v1.1.0) (2024-03-19)


### Features

* Add role seeder ([e5a095c](https://gitlab.com/aredn-apps/meshphone-db/commit/e5a095c66428015962b7923ff1f03c505899f873))


### Bug Fixes

* Fixed dialplan permission names ([7892202](https://gitlab.com/aredn-apps/meshphone-db/commit/789220291e968d687fa04089586c090b6f449bf4))

## [1.0.4](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.0.3...v1.0.4) (2024-03-16)


### Bug Fixes

* Add dialplan permissions ([0a9696d](https://gitlab.com/aredn-apps/meshphone-db/commit/0a9696d979162f0420e2b2d55fa184e181650050))
* Fix display of delete functions for admins ([57175ab](https://gitlab.com/aredn-apps/meshphone-db/commit/57175ab95cdba6173af2274813dc7ceae0934784))
* Fix guests view of search results ([563aa65](https://gitlab.com/aredn-apps/meshphone-db/commit/563aa650b7c57f819f8fe6c5c27430815372066c))

## [1.0.3](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.0.2...v1.0.3) (2024-03-16)

## [1.0.2](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.0.1...v1.0.2) (2024-03-16)

## [1.0.1](https://gitlab.com/aredn-apps/meshphone-db/compare/v1.0.0...v1.0.1) (2024-03-16)


### Bug Fixes

* Fix password change procedure ([790b4ce](https://gitlab.com/aredn-apps/meshphone-db/commit/790b4ce0f21a2249cad6cc34844311d48aeb202e))

## 1.0.0 (2024-03-16)


### Features

* Add admin name to search results ([d2e06e3](https://gitlab.com/aredn-apps/meshphone-db/commit/d2e06e3acfbb19a3ee366872cb024e934800ea6b))
* Add controls based on user role ([93c42e0](https://gitlab.com/aredn-apps/meshphone-db/commit/93c42e01b56b7a2bf384e0cac52557cdb72de800))
* Add user perms to MeshPhone panel ([654cce6](https://gitlab.com/aredn-apps/meshphone-db/commit/654cce6ce17836b2ec1e786992de71d39763792e))
* Implement spatie/laravel-permissions roles and permissions ([fc87b92](https://gitlab.com/aredn-apps/meshphone-db/commit/fc87b92fd30dc3657a1aeb9dfd270cbf1b9e3e08))
* Update app name to MeshPhone DB ([c003af8](https://gitlab.com/aredn-apps/meshphone-db/commit/c003af8ac8a802d22f0e8205895fdf80ce97d952))


### Bug Fixes

* Fix delete button on user index page ([36d0ea4](https://gitlab.com/aredn-apps/meshphone-db/commit/36d0ea47d6694857c8bb1314ed8ceb9ec88930d5))
* Fix views to display using new roles and permissions ([3439eaa](https://gitlab.com/aredn-apps/meshphone-db/commit/3439eaa157bd356c5f9d5e63ebb84184028fb071))
