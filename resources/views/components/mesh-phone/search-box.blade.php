<div @class(['border-2', 'border-slate-700', 'bg-yellow-100'])>
    <div class="m-2 font-bold">
        Searching for a PBX will partially match the PBX name,
        administrator contact information, administrator call sign,
        location, member network, hostname and IP address.

        Searching for office code will match the full office code.
    </div>
    <form method="GET" action="/meshphone/search">
        <input type="text" id="search_for" name="search_for" value=""
            @class(['rounded-md', 'm-4'])>
        <x-primary-button class="mt-4">{{ __('Find PBX') }}</x-primary-button>
        <a href="{{ route('meshphone.search', ['search_for' => '%']) }}" class="inline-block ml-2">
            <x-secondary-button>{{ __('Show all PBXes') }}</x-secondary-button>
        </a>
    </form>
</div>
