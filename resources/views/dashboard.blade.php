<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-lg sm:rounded-lg">
            <form method="GET" action="/meshphone/search">
                <div class="mx-24">
                    Quick search:
                    <input type="text" id="search_for" name="search_for" value=""
                    @class(['rounded-md', 'm-4'])>
                    <x-primary-button class="mt-4">{{ __('Find PBX') }}</x-primary-button>
                    <a href="{{ route('meshphone.search', ['search_for' => '%']) }}" class="inline-block ml-2">
                        <x-secondary-button>{{ __('Show all PBXes') }}</x-secondary-button>
                    </a>
                </div>
            </form>
            </div>
        </div>

        <div class="max-w-7xl my-4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-lg sm:rounded-lg">
                <table class="my-8 mx-auto border-separate border-spacing-1">
                    <tr class="border-collapse border-spacing-0">
                        <td colspan='4', class="p-4 m-4 bg-gray-500 text-white text-2xl text-center">
                        PBX Status:
                    </td>
                    <td colspan='3' class="p-4 bg-gray-500 text-white text-xl text-center">
                        Total: {{ $total }}
                    </td></tr>
                    <tr>
                        <td class="p-4 bg-green-200 rounded-lg text-center">
                            Active<br/>
                            <span class="text-xl">{{ $status_count['Active'] ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-indigo-200 rounded-lg text-center">
                            Inactive<br/>
                            <span class="text-xl">{{ $status_count['Inactive']  ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-red-200 rounded-lg text-center">
                            Maintenance<br/>
                            <span class="text-xl">{{ $status_count['Maintenance'] ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-green-200 rounded-lg text-center">
                            Application<br/>
                            <span class="text-xl">{{ $status_count['Application'] ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-yellow-200 rounded-lg text-center">
                            Provisioning<br/>
                            <span class="text-xl">{{ $status_count['Provisioning'] ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-purple-200 rounded-lg text-center">
                            Decommision<br/>
                            <span class="text-xl">{{ $status_count['Decommission'] ?? 0 }}</span>
                        </td>
                        <td class="p-4 bg-gray-300 rounded-lg text-center">
                            Unknown<br/>
                            <span class="text-xl">{{ $status_count['Unknown'] ?? '0' }}</span>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
        </div>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">

                    <script>

                        var data = {!! json_encode($status_count) !!};

                        chart = {
                            const height = Math.min(width, 500);
                            const radius = Math.min(width, height) / 2;

                            const arc = d3.arc()
                                .innerRadius(radius * 0.67)
                                .outerRadius(radius - 1);

                            const pie = d3.pie()
                                .padAngle(1 / radius)
                                .sort(null)
                                .value(d => d.value);

                            const color = d3.scaleOrdinal()
                                .domain(data.map(d => d.name))
                                .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length).reverse());

                            const svg = d3.create("svg")
                                .attr("width", width)
                                .attr("height", height)
                                .attr("viewBox", [-width / 2, -height / 2, width, height])
                                .attr("style", "max-width: 100%; height: auto;");

                            svg.append("g")
                              .selectAll()
                              .data(pie(data))
                              .join("path")
                                .attr("fill", d => color(d.data.name))
                                .attr("d", arc)
                              .append("title")
                                .text(d => `${d.data.name}: ${d.data.value.toLocaleString()}`);

                            svg.append("g")
                                .attr("font-family", "sans-serif")
                                .attr("font-size", 12)
                                .attr("text-anchor", "middle")
                              .selectAll()
                              .data(pie(data))
                              .join("text")
                                .attr("transform", d => `translate(${arc.centroid(d)})`)
                                .call(text => text.append("tspan")
                                    .attr("y", "-0.4em")
                                    .attr("font-weight", "bold")
                                    .text(d => d.data.name))
                                .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
                                    .attr("x", 0)
                                    .attr("y", "0.7em")
                                    .attr("fill-opacity", 0.7)
                                    .text(d => d.data.value.toLocaleString("en-US")));

                            return svg.node();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
