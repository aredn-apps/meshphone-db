<x-app-layout>
<div class="my-4">
    <div class="field-container mx-8">
        <div class="pull-left">
            <h2 class="mx-8">Users Management</h2>
        </div>
        <div class="pull-right">
            <a class="inline-block ml-2" href="{{ route('users.create') }}">
                <x-secondary-button>{{ __('Create new user') }}</x-secondary-button>
            </a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<table class="table-auto *:border *:border-slate-600 my-8 mx-auto w-3/4">
 <tr class="*:border *:border-slate-600 *:w-2 *:bg-sky-200">
   <th>No</th>
   <th>Name</th>
   <th>Call sign</th>
   <th>Email</th>
   <th>Roles</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $user)
  <tr class="*:border *:border-slate-600 *:p-2">
    <td>{{ ++$i }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->callsign }}</td>
    <td>{{ $user->email }}</td>
    <td>
      @if(!empty($user->getRoleNames()))
        @foreach($user->getRoleNames() as $v)
           <label class="badge badge-success">{{ $v }}</label>
        @endforeach
      @endif
    </td>
    <td>
        <a href="{{ route('users.edit',$user->id) }}" class="inline-block mx-2">
            <x-primary-button>Edit</x-primary-button>
        </a>
        <a href="{{ route('users.destroy', $user->id) }}" class="inline-block mx-2">
            <x-danger-button name="delete" value="delete" >Delete</x-danger-button>
        </a>
    </td>
  </tr>
 @endforeach
</table>

</x-app-layout>
