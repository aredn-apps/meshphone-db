@php
    use Spatie\Permission\Models\Role;
    //$user = auth()->user();
    $all_roles = Role::all();
@endphp
<x-app-layout>
<div class="my-4">
    <div class="field-container mx-8">
        <div class="pull-left">
            <h2 class="mx-8">Edit User</h2>
        </div>
        <div class="pull-right">
            <a class="inline-block ml-2" href="{{ route('users.index') }}">
                <x-secondary-button>{{ __('Back') }}</x-secondary-button>
            </a>
        </div>
    </div>
</div>

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif

<form method="POST" action="{{ route('users.update', $user->id) }}">
@csrf
@method('patch')
<table class="table-fixed mt-8 border-gray-500 mx-auto w-3/4">
    <tr><td>
        <label for="name">Name:
            @error('name')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('name') ? 'ring-4 ring-red-500' :'' }}"
                type="text" required id="name" name="name"
                value="{{ old('name', $user->name) }}">
        </label>
    </td><td>
    <tr><td>
        <label for="email">Email:
            @error('email')
                <span class="font-bold teZxt-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('email') ? 'ring-4 ring-red-500' :'' }}"
                type="email" required id="email" name="email"
                value="{{ old('email', $user->email) }}">
        </label>
    </td><td>
    <tr><td>
        <label for="password">Password:
            @error('password')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('password') ? 'ring-4 ring-red-500' :'' }}"
                type="password" id="password" name="password"
                value="{{ old('password', $user->password) }}">
        </label>
    </td><td>
    <tr><td>
        <label for="confirm-password">Confirm password:
            @error('confirm-password')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('confirm-password') ? 'ring-4 ring-red-500' :'' }}"
                type="password" id="confirm-password" name="confirm-password"
                value="{{ old('confirm-password', $user->confirm_password) }}">
        </label>
    </td><td>
    <tr><td>
        <label for="callsign">Callsign:
            @error('callsign')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('callsign') ? 'ring-4 ring-red-500' :'' }}"
                type="text" required id="callsign" name="callsign"
                value="{{ old('callsign', $user->callsign) }}">
        </label>
    </td><td>
    <tr><td>
        <label for="roles">Roles:
            @error('roles')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <select name="roles[]" id="roles"
                class="rounded-md p-2 border border-slate-600 focus:ring-2 invalid:border-red-500"
                multiple>
                @foreach ($all_roles as $role)
                    <option value="{{ $role->name }}"
                        {{ (old('roles') === $role->name || $user->hasRole($role->name)) ? 'selected' : '' }}>
                        {{ $role->name }}
                    </option>
                @endforeach
            </select>
        </label>
    </td></tr>
    <tr><td>

    <div class="button-container">
        <div class="left-buttons">
            <x-primary-button class="mt-4">{{ __('Submit') }}</x-primary-button>
            <a href="{{ route('users.index') }}" class="inline-block ml-2">
                <x-secondary-button>{{ __('Cancel') }}</x-secondary-button>
            </a>
        </div>
        <div class="right-buttons">
            <x-danger-button name="delete" value="delete" class="mt-4 ml-2">{{ __('Delete User') }}</x-danger-button>
        </div>
    </div>
    </td></tr>
</table>
</form>
</x-app-layout>
