<x-app-layout>
    <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
        @auth
        <form method="get" action="{{ route('meshphone.index') }}">
        @else
        <form method="get" action="/">
        @endauth

            @include('meshphone/partials/meshphone', ['edit' => 0])

            <x-primary-button class="mt-4">{{ __('Return') }}</x-primary-button>
        </form>
    </div>
</x-app-layout>
