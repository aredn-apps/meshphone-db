@use('App\Models\PbxPermission')
<x-app-layout>
    <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
        <form method="POST" action="{{ route('meshphone.update', $pbx) }}">
            @csrf
            @method('patch')
            @include('meshphone/partials/meshphone', ['edit' => 1])


            <div class="button-container">
                <div class="left-buttons">
                    <x-primary-button class="mt-4">{{ __('Submit') }}</x-primary-button>
                    <a href="{{ route('meshphone.index') }}" class="inline-block ml-2">
                        <x-secondary-button>{{ __('Cancel') }}</x-secondary-button>
                    </a>
                </div>
                <div class="right-buttons">
                    @can(PbxPermission::DeletePBX)
                    <x-danger-button name="delete" value="delete" class="mt-4 ml-2">{{ __('Delete PBX') }}</x-danger-button>
                    @endcan
                </div>
            </div>
        </form>
    </div>
</x-app-layout>
