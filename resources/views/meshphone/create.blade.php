<x-app-layout>
    <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
        <form method="POST" name="create_meshphone" id="create_meshphone" action="{{ route("meshphone.store") }}" >
            @csrf
            @include('meshphone/partials/meshphone', ['pbx' => $pbx])

            <x-primary-button class="mt-4">{{ __('Submit') }}</x-primary-button>
            <a href="{{ route('meshphone.index') }}">{{ __('Cancel') }}</a>
    </form>

    </div>
</x-app-layout>
