@use('App\Models\PbxStatus')
@use('App\Models\PbxPermission')
@use('Illuminate\Support\Arr')
<x-app-layout>
    <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
        <x-input-error :messages="$errors->get('message')" class="mt-2" />
        @if ($errors->any())
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            @endif
        @if(session()->has('success'))
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <p class='font-xl text-green-600'>{{ session()->get('success') }}</p>
            </div>
        @endif

        <br/>
        @can(PbxPermission::CreatePBX)
        <a href="{{ route("meshphone.create") }}"><x-primary-button class="mt-4">{{ __('Add new PBX') }}</x-primary-button></a>
        @endcan
        <x-mesh-phone.search-box/>
    </div>

    @if( ! empty($results))
    <table class="table-auto *:border *:border-slate-600 my-8 mx-auto w-3/4">
        <caption class="font-extrabold text-2xl">Search results</caption>
        <tr class="*:border *:border-slate-600 *:w-2 *:bg-sky-200">
            <th>PBX name</th><th>PBX status</th><th>Location</th><th>Admin</th><th>Hostname</th>
        </tr>
        @foreach($results as $entry)
        <tr class="*:border *:border-slate-600 *:p-2"><td>
            @auth
                <a class="text-blue-600 hover:underline" href="{{ route('meshphone.edit', $entry) }}">{{ $entry->name }}</a>
            @else
                <a class="text-blue-600 hover:underline" href="{{ route('meshphone.show', $entry) }}">{{ $entry->name }}</a>
            @endauth
        </td>
        @if($entry->pbx_status == PbxStatus::Inactive->name)
        <td class="bg-indigo-200">
        @elseif($entry->pbx_status == PbxStatus::Active->name)
        <td class="bg-green-200">
        @elseif($entry->pbx_status == PbxStatus::Unknown->name)
        <td class="bg-gray-300">
        @elseif($entry->pbx_status == PbxStatus::Maintenance->name)
        <td class="bg-red-200">
        @elseif($entry->pbx_status == PbxStatus::Provisioning->name)
        <td class="bg-yellow-200">
        @elseif($entry->pbx_status == PbxStatus::Decommission->name)
        <td class="bg-purple-200">
        @else
        <td>
        @endif
            {{ $entry->pbx_status }}
            @if (Arr::exists($entry, 'checkin_at') && $entry->checkin_at->DiffInHours(now()) < 8)
                @svg('uiw-cloud-upload', 'icon px-1')
            @elseif (Arr::exists($entry, 'checkin_at'))
                @svg('uiw-cloud-upload-o', 'icon px-1')
            @endif
            @if (count($entry->trunks) == 0)
                @svg('uiw-stop', 'icon px-1')
            @endif
        </td><td>
            {{ $entry->location }}
        </td><td>
            {{ $entry->admin_name }}, {{ $entry->admin_callsign }}
            <a href="mailto:{{ $entry->admin_email }}?subject=MeshPhone%20Request">
                @svg('uiw-mail-o', 'icon px-2')
            </a>
        </td><td>
            {{ $entry->hostname }}
        </td></tr>
        @endforeach
    </table>

    <table class="table-auto *:border *:border-slate-600 my-8 mx-auto w-3/4">
        <tr>
            <td>@svg('uiw-cloud-upload', 'icon px-1') PBX has checked in within the last 8 hours</td>
            <td>@svg('uiw-cloud-upload-o', 'icon px-1') PBX checked in over 8 hours ago</td>
            <td>@svg('uiw-stop', 'icon px-1') PBX has no trunks</td>
        </tr>
    </table>
    @endif


</x-app-layout>
