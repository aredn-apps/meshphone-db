@use('App\Helpers\Helper')
@use('App\Models\PbxPermission')
@php
    $user = auth()->user();
    $pbx_name_list = Arr::sort(Arr::where(Helper::pbxNameList(),
        function(string $name, int $key) use ($pbx) {
            return $name != $pbx->name;
    }));
@endphp
<!--******************************************************************
    Contact information
 *******************************************************************-->
<table class="table-fixed mt-8 border-gray-500">
    <caption class="font-extrabold text-2xl">Contact information</caption>
    <tr><td>
        <label for="admin_name">Admin name:
            @error('admin_name')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="peer/admin_name rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('admin_name') ? 'ring-4 ring-red-500' :'' }}"
                type="text" required id="admin_name" name="admin_name"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateContactInfo) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('admin_name', $pbx->admin_name) }}">
        </label>
    </td><td>
        &nbsp;
    </td><td>
        <label for="admin_email">Email:
            @error('admin_email')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('admin_email') ? 'ring-4 ring-red-500' :'' }}"
                type="email" required id="admin_email" name="admin_email"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateContactInfo) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('admin_email', $pbx->admin_email) }}">
        <label>
    </td></tr>
    <tr><td>
        <label for="admin_callsign">Callsign:
            @error('admin_callsign')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md uppercase focus:ring-2 invalid:border-red-500
                {{ $errors->has('admin_callsign') ? 'ring-4 ring-red-500' :'' }}"
                type="text" required id="admin_callsign" name="admin_callsign"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateContactInfo) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('admin_callsign', $pbx->admin_callsign) }}">
        </label>
    </td><td>
        &nbsp;
    </td><td>
        <label for="admin_phone">MeshPhone:
            @error('admin_phone')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('admin_phone') ? 'ring-4 ring-red-500' :'' }}"
                type="tel" pattern="[0-9]{3}-[0-9]{4}"
                required id="admin_phone" name="admin_phone"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateContactInfo) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('admin_phone', $pbx->admin_phone) }}">
        </label>
    </td></tr>
</table>
<!--********************************************************************
    Location
 ********************************************************************-->
<table class="table-fixed mt-8">
    <caption class="font-extrabold text-2xl">Location</caption>
    <tr><td colspan="3">
        <label for="location">Location:
            @error('location')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('location') ? 'ring-4 ring-red-500' :'' }}"
                required type="text" id="location" name="location"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateLocation) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('location', $pbx->location) }}">
        </label>
    </td><tr>
    <tr><td>
        <label for="latitude">Latitude:
            @error('latitude')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('latitude') ? 'ring-4 ring-red-500' :'' }}"
                required type="number" step="0.00001" min="-90" max="90"
                id="latitude" name="latitude"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateLocation) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('latitude', $pbx->latitude) }}">
        </label>
    </td><td>
        &nbsp;
    </td><td>
        <label for="longitude">Longitude:
            @error('longitude')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('longitude') ? 'ring-4 ring-red-500' :'' }}"
                required type="number" step="0.00001" min="-180" max="180"
                id="longitude" name="longitude"
                @if ($edit == 0 || $user->cannot(PbxPermission::UpdateLocation) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('longitude', $pbx->longitude) }}">
        </label>
    </td></tr>
</table>
<p/>
<!--********************************************************************
    PBX information
 ********************************************************************-->
<table class="table-fixed mt-8">
    <caption class="font-extrabold text-2xl">PBX information</caption>
    <tr><td>
        <label for="name">Name: (No spaces or callsigns)
            @error('name')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md uppercase focus:ring-2 invalid:border-red-500
                {{ $errors->has('name') ? 'ring-4 ring-red-500' :'' }}"
                required type="text" id="name" name="name"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX))
                disabled
                @endif
                value="{{ old('name', $pbx->name) }}">
            <input type="hidden" name='orig_name' value="{{ old('orig_name', $pbx->name) }}">
        </label>
    </td><td>
        <label for="network">Network:
            @error('network')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('network') ? 'ring-4 ring-red-500' :'' }}"
                type="text" id="network" name="network"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX))
                disabled
                @endif
                value="{{ old('network', $pbx->network) }}">
        </label>
    </td><td>
        <label for="pbx_type">PBX Type:
            @error('pbx_type')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <select class="rounded-md p-2 border border-slate-600 focus:ring-2 invalid:border-red-500"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                id="pbx_type" name="pbx_type">
                @use('App\Models\PbxType')
                @foreach (PbxType::cases() as $type)
                    <option value="{{ $type->name }}" {{ old('pbx_status', $pbx->pbx_type) === $type->name ? 'selected' : '' }}>{{ $type->name }}</option>
                @endforeach
            </select>
        </label>
    </td></tr>
    <tr><td>
        <label for="hostname">Hostname:
            @error('nostname')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('hostname') ? 'ring-4 ring-red-500' :'' }}"
                type="text" id="hostname" name="hostname"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('hostname', $pbx->hostname) }}">
        </label>
    </td><td>
        <label for="ip_address">IP Address:
            @error('ip_address')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has('ip_address') ? 'ring-4 ring-red-500' :'' }}"
                type="text" id="ip_address" name="ip_address"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX) && $user->callsign != $pbx->admin_callsign)
                disabled
                @endif
                value="{{ old('ip_address', $pbx->ip_address) }}">
        </label>
    </td><td>
        <label for="pbx_status">PBX Status:
            @error('pbx_status')
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <select class="rounded-md p-2 border border-slate-600 focus:ring-2 invalid:border-red-500"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditPBX))
                disabled
                @endif
                id="pbx_status" name="pbx_status">
                @use('App\Models\PbxStatus')
                @foreach (PbxStatus::cases() as $status)
                    <option value="{{ $status->name }}" {{ old('pbx_status', $pbx->pbx_status) === $status->name ? 'selected' : '' }}>{{ $status->name }}</option>
                @endforeach
            </select>
    </td></tr>
    <tr><td>
        @php
            $color_style = Helper::DateColor($pbx->checkin_at);
        @endphp
        <label for="checkin_at">Last Checkin:
            <input class="rounded-md px-2" style="{{ $color_style }}" type="timestamp" id="created_at"
                name="checkin_at"
                value="{{ $pbx->checkin_at ? ($pbx->checkin_at . "  UTC") : "Never" }}">
        </label>
        </td><td colspan="2">
            <div class="mt-6 *:mx-1">
                <span style='background-color: lightgreen; color: charcoal;'>&lt; 8 hours</span>
                <span style='background-color: yellow; color: charcoal;'>&lt; 24 hours</span>
                <span style='background-color: #FFBF00; color: charcoal;'>&lt; 72 hours</span>
                <span style='background-color: #C21E56; color: linen;'>&gt;  3 days</span>
            </div>
    </td></tr>
    @if ($pbx->gen_script_name)
    <tr><td>
            <label for="script_name_version">Generation script and version:
        </td><td colspan='2'>
            <div class='rounded-md px-2 text-white bg-slate-500'>
                {{ $pbx->gen_script_name }} : {{ $pbx->gen_script_version }}
            </div>
        </td>
    </tr>
    @endif
</table>

<?php $next_office_code_index = 0; ?>
<table class="table-fixed mt-8" id="office_codes">
    <caption class="font-extrabold text-2xl">Office codes</caption>
    <tr class="*:border *:border-slate-600 *:w-2 *:bg-sky-200">
        <th>Office code</th><th>NPA</th><th>Dialplans</th>
    </tr>
    @foreach(old('office_codes', $pbx->office_codes) as $entry)
        <tr class="*:border *:border-slate-600 *:p-2"><td class="whitespace-nowrap">
            <?php $field = "office_codes[$loop->index][office_code]"; ?>
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has($field) ? 'ring-4 ring-red-500' :'' }}"
                required type="text" id="{{ $field }}" name="{{ $field }}"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditOfficecode))
                disabled
                @endif
                size="5" value="{{ old($field, $entry['office_code']) }}">
            @if ($edit == 1 && $user->can(PbxPermission::DeleteOfficecode))
            @svg('uiw-delete', 'icon px-2 removeOfficeCode')
            @endif
        </td><td>
            <?php $field = "office_codes[$loop->index][npa]"; ?>
            <input class="rounded-md focus:ring-2 invalid:border-red-500
                {{ $errors->has($field) ? 'ring-4 ring-red-500' :'' }}"
                size="3" required type="text" id="{{ $field }}"
                @if ($edit == 0 || $user->cannot(PbxPermission::EditOfficecode))
                disabled
                @endif
                name="{{ $field }}" value="{{ old($field, $entry['npa']) }}">
        </td><td>
            @php
            $outer = "office_codes[$loop->index]";
            $dialplan_field = "dialplans_{$loop->index}";
            @endphp
            <table id="{{ $dialplan_field }}">
            @error($dialplan_field)
                <span class="font-bold text-red-700" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            @if (array_key_exists('dialplans', $entry))
                @foreach($entry['dialplans'] as $dialplan)
                <tr><td class="whitespace-nowrap">
                    <?php $field = $outer."[dialplans][]"; ?>
                    <input class="rounded-md focus:ring-2 invalid:border-red-500
                    {{ $errors->has($dialplan_field) ? 'ring-4 ring-red-500' :'' }}"
                    required type="text" size="10" id="{{ $field }}"
                    @if ($edit == 0 || $user->cannot(PbxPermission::EditDialplan))
                    disabled
                    @endif
                    name="{{ $field }}" value="{{ old($field, $dialplan) }}">
                    @if ($edit == 1 && $user->can(PbxPermission::DeleteDialplan))
                    @svg('uiw-delete', 'icon px-2 removeDialplan')
                    @endif
                </td></tr>
                @endforeach
            @endif
            @if ($edit == 1 && $user->can(PbxPermission::CreateDialplan))
                <tr><td>
                    <button class="border border-slate-600 rounded text-white text-xs bg-indigo-600 mx px pr-2"
                        type="button" id='addDialplan{{ $loop->index }}'
                        onclick="addDialplan(this)">
                            @svg('uiw-plus-circle-o', 'icon px-2') Add dialplan
                    </button>
                </td></tr>
            @endif
            </table>
        </td></tr>
        @if($loop->last)
            <?php $next_office_code_index = $loop->iteration; ?>
        @endif
    @endforeach
</table>
@if ($edit == 1 && $user->can(PbxPermission::CreateOfficecode))
<!-- Add button for office code entry -->
<button class="border border-slate-600 rounded text-white bg-indigo-600 m-2 p-2"
    type="button" id="addOfficeCode">
        @svg('uiw-plus-circle-o', 'icon px-2') Add office code
</button>
@endif

<?php $next_trunk_index = 0; ?>
<table class="table-fixed mt-8" id="trunks">
    <caption class="font-extrabold text-2xl">Trunks</caption>
    <tr class="*:border *:border-slate-600 *:p-2 *:bg-sky-200">
        <th>PBX name</th><th>Office code</th><th>Latency (ms)</th></tr>
    @foreach(old('trunks', $pbx->trunks) as $entry)
        <tr class="*:border *:border-slate-600 *:p-2"><td class="whitespace-nowrap">
            @php
                $field = "trunks[$loop->index][pbx_name]";
                if (array_key_exists('pbx_name', $entry)){
                    $remote = Helper::pbxInfo($entry['pbx_name']);
                    $pbx_name = $entry['pbx_name'];
                } else {
                    $remote = Helper::locateOfficeCode($entry['office_code']);
                    $pbx_name = $remote ? $remote->name : '';
                }
            @endphp
            @if($remote)
            <div class="field-container"><div class="left">
                <a href="{{ route('meshphone.edit', $remote->_id) }}"
                    class="text-blue-500">
                    {{ $pbx_name }}
                    @svg('uiw-link', 'icon px-2')
                </a>
            </div><div class="right">
                @if ($edit == 1 && $user->can(PbxPermission::DeleteTrunk))
                @svg('uiw-delete', 'icon px-2 removeTrunk')
                @endif
                <input type='hidden' name='{{ $field }}' value='{{ $pbx_name }}'/>
            </div></div>
            @endif
        </td><td class="whitespace-nowrap text-center">
            @php
                $field = "trunks[$loop->index][office_code]";
                if ($pbx_name) {
                    $office_code = Helper::firstOfficeCode($pbx_name);
                } else {
                    $office_code = $entry['office_code'];
                }
            @endphp
            {{ $office_code }}
            <input type='hidden' name='{{ $field }}' value='{{ $office_code }}'/>
        </td><td class="whitespace-nowrap text-center">
            @if (array_key_exists('latency', $entry))
                @if ($entry['latency_history'][0] == 9999)
                    <div class="bg-red-600 text-white">DOWN</div>
                @else
                    {{ $entry['latency'] }}
                @endif
            @endif
        </td></tr>
        @if($loop->last)
            <?php $next_trunk_index = $loop->iteration; ?>
        @endif
    @endforeach
</table>
<!-- Add button for creating a tunk -->
@if ($edit == 1 && $user->can(PbxPermission::CreateTrunk))
<button class="border border-slate-600 rounded text-white bg-indigo-600 m-2 p-2"
    type="button" id="addTrunk">
    @svg('uiw-plus-circle-o', 'icon px-2') Add trunk
</button>
@endif

@if( ! empty($pbx) and $pbx->_id)
<table class="table-fixed mt-8">
    <caption class="font-extrabold text-2xl">DB record metadata</caption>

    <tr><td colspan="3">
        <label for="id">ID:
            <input class="rounded-md px-2" type="timestamp" id="mongodb_id" name="mongodb_id" value="{{ old('_id', $pbx->_id) }}" disabled>
            <input type="hidden" id="_id" name="_id" value="{{ $pbx->_id }}">
        </label>
    </td></tr>
    <tr><td>
        <label for="created">Created at:
            <input class="rounded-md px-2" type="timestamp" id="created_at" name="created_at" value="{{ $pbx->created_at }}  UTC" disabled>
        </label>
    </td><td>
        &nbsp;
    </td><td>
        <label for="updated">Updated at:
            <input class="rounded-md px-2" type="timestamp" id="updated_at" name="updated_at" value="{{ $pbx->updated_at }}  UTC" disabled>
        </label>
    </td></tr>
</table>
@endif

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        let next_office_code_index={{ $next_office_code_index }};
        let next_trunk_index={{ $next_trunk_index }};
        let pbx_name_list=[ '{!! implode("', '", $pbx_name_list) !!}' ];

        addField = (field_name, size=10) => {
            let css = "rounded-md focus:ring-2 invalid:border-red-500";
            return "<input class='"+css+"' required type='text' size='"+size+"' id='"+field_name+"' name='"+field_name+"' value=''>";
        }

        svgDeleteIcon = (class_name) => {
            return '<svg class="icon px-2 '+class_name+'" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"><path fill="currentColor" d="M9.12856092,0 L11.102803,0.00487381102 C11.8809966,0.0985789507 12.5627342,0.464975115 13.1253642,1.0831551 C13.583679,1.58672038 13.8246919,2.17271137 13.8394381,2.81137259 L19.3143116,2.81154887 C19.6930068,2.81154887 20,3.12136299 20,3.50353807 C20,3.88571315 19.6930068,4.19552726 19.3143116,4.19552726 L17.478,4.195 L17.4783037,15.8224356 C17.4783037,18.3654005 16.529181,20 14.4365642,20 L5.41874994,20 C3.32701954,20 2.39315828,18.3737591 2.39315828,15.8224356 L2.393,4.195 L0.685688428,4.19552726 C0.306993166,4.19552726 0,3.88571315 0,3.50353807 C0,3.12136299 0.306993166,2.81154887 0.685688428,2.81154887 L6.15581653,2.81128823 C6.17048394,2.29774844 6.36057711,1.7771773 6.7098201,1.26219866 C7.23012695,0.494976667 8.04206594,0.0738475069 9.12856092,0 Z M16.106,4.195 L3.764,4.195 L3.76453514,15.8224356 C3.76453514,17.7103418 4.28461756,18.6160216 5.41874994,18.6160216 L14.4365642,18.6160216 C15.5759705,18.6160216 16.1069268,17.7015972 16.1069268,15.8224356 L16.106,4.195 Z M6.71521035,6.34011422 C7.09390561,6.34011422 7.40089877,6.64992834 7.40089877,7.03210342 L7.40089877,15.0820969 C7.40089877,15.464272 7.09390561,15.7740861 6.71521035,15.7740861 C6.33651508,15.7740861 6.02952192,15.464272 6.02952192,15.0820969 L6.02952192,7.03210342 C6.02952192,6.64992834 6.33651508,6.34011422 6.71521035,6.34011422 Z M9.44248307,6.34011422 C9.82117833,6.34011422 10.1281715,6.64992834 10.1281715,7.03210342 L10.1281715,15.0820969 C10.1281715,15.464272 9.82117833,15.7740861 9.44248307,15.7740861 C9.06378781,15.7740861 8.75679464,15.464272 8.75679464,15.0820969 L8.75679464,7.03210342 C8.75679464,6.64992834 9.06378781,6.34011422 9.44248307,6.34011422 Z M12.1697558,6.34011422 C12.5484511,6.34011422 12.8554442,6.64992834 12.8554442,7.03210342 L12.8554442,15.0820969 C12.8554442,15.464272 12.5484511,15.7740861 12.1697558,15.7740861 C11.7910605,15.7740861 11.4840674,15.464272 11.4840674,15.0820969 L11.4840674,7.03210342 C11.4840674,6.64992834 11.7910605,6.34011422 12.1697558,6.34011422 Z M9.17565461,1.38234438 C8.53434679,1.42689992 8.11102741,1.64646338 7.84152662,2.04385759 C7.6437582,2.33547837 7.5448762,2.58744977 7.52918786,2.81194335 L12.4673768,2.81085985 C12.4530266,2.51959531 12.3382454,2.26423777 12.1153724,2.01935991 C11.7693001,1.63911901 11.3851686,1.43266964 11.0215648,1.38397839 L9.17565461,1.38234438 Z"/></svg>';
        }

        addSelection = (field_name, options) => {
            let css = "rounded-md p-2 border border-slate-600 focus:ring-2 invalid:border-red-500";
            let row = "<select class='"+css+"' id='"+field_name+"' name='"+field_name+"'>";
            for(name of options) {
                row += "<option value='"+name+"'>"+name+"</option>";
            }
            row += "</select>";

            return row;
        }

        addOfficeCodeRow = () => {
            let current_index = next_office_code_index.toString();
            let row = "<tr class='*:border *:border-slate-600 *:p-2'><td class='whitespace-nowrap'>";
            row += addField("office_codes["+current_index+"][office_code]", 5);
            row += svgDeleteIcon('removeOfficeCode');
            row += "</td><td>";
            row += addField("office_codes["+current_index+"][npa]", 3);
            row += "</td><td>";
            row += "<table id='dialplan_"+current_index+"'>";
            row += addDialplanRow(current_index);
            row += "<tr><td>"+addDialplanButton(current_index)+"</td></tr>";
            row += "</table></td></tr>";
            next_office_code_index++;
            return row;
        }

        addTrunkRow = () => {
            let current_index = next_trunk_index.toString();
            let row = "<tr class='*:border *:border-slate-600 *:p-2'>";
            row += "<td class='whitespace-nowrap'>";
            row += "<div class='field-container'><div class='left'>";
            row += addSelection("trunks["+current_index+"][pbx_name]", pbx_name_list);
            row += "</div><div class='right'>"
            row += svgDeleteIcon('removeTrunk');
            row += "</div>";
            row += "</td><td class='whitespace-nowrap'>";
            row += "&nbsp;";
            row += "</td><td class='whitespace-nowrap'>";
            row += "</td></tr>";
            next_trunk_index++;
            return row;
        }

        addDialplanRow = (entry_num) => {
            let row = "<tr><td class='whitespace-nowrap'>"
            row += addField("office_codes["+entry_num+"][dialplans][]", 10);
            row += svgDeleteIcon('removeDialplan');
            row += "</td></tr>";
            return row;
        }

        addDialplanButton = (entry_num) => {
            return "<button class='border border-slate-600 rounded text-white text-xs bg-indigo-600 mx px pr-2' type='button' id='addDialplan"+entry_num+"' onclick='addDialplan(this)'><svg class='icon px-2' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20' fill='currentColor'><path fill='currentColor' d='M10,0 C15.5228475,0 20,4.4771525 20,10 C20,15.5228475 15.5228475,20 10,20 C4.4771525,20 0,15.5228475 0,10 C0,4.4771525 4.4771525,0 10,0 Z M10,1.39534884 C5.24778239,1.39534884 1.39534884,5.24778239 1.39534884,10 C1.39534884,14.7522176 5.24778239,18.6046512 10,18.6046512 C14.7522176,18.6046512 18.6046512,14.7522176 18.6046512,10 C18.6046512,5.24778239 14.7522176,1.39534884 10,1.39534884 Z M10,5.47455848 C10.3765578,5.47455848 10.6818182,5.77981887 10.6818182,6.15637666 L10.681,9.279 L13.8050335,9.27959198 C14.1815913,9.27959198 14.4868517,9.58485238 14.4868517,9.96141017 C14.4868517,10.3379679 14.1815913,10.6432283 13.8050335,10.6432283 L10.681,10.643 L10.6818182,13.7664437 C10.6818182,14.1430015 10.3765578,14.4482619 10,14.4482619 C9.62344222,14.4482619 9.31818182,14.1430015 9.31818182,13.7664437 L9.318,10.643 L6.19496649,10.6432283 C5.81840871,10.6432283 5.51314831,10.3379679 5.51314831,9.96141017 C5.51314831,9.58485238 5.81840871,9.27959198 6.19496649,9.27959198 L9.318,9.279 L9.31818182,6.15637666 C9.31818182,5.77981887 9.62344222,5.47455848 10,5.47455848 Z'/></svg>Add dialplan</button>";
        }

        $('#addOfficeCode').click(() => {
            $('#office_codes').append(addOfficeCodeRow());
        });
        $('#addTrunk').click(() => {
            $('#trunks').append(addTrunkRow());
        });
        addDialplan = (elem) => {
            let button = $('#'+elem.id);
            let table = button.parent().parent().parent().parent();
            let entry_num = table[0].id.replace('dialplans_', '');
            $(addDialplanRow(entry_num)).insertBefore(button.parent().parent());
        };


        // Dynamically remove sub-component entry
        $(document).on('click', '.removeOfficeCode', function() {
            $(this).parent().closest('tr').remove();
        });
        $(document).on('click', '.removeDialplan', function() {
            $(this).parent().closest('tr').remove();
        });
        $(document).on('click', '.removeTrunk', function() {
            $(this).closest('tr').remove();
        });
    });
</script>
