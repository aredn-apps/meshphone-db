<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MeshPhoneController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Helpers\Helper;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // frontpage.md needs to reside in the public directory
    if (file_exists('frontpage.md')) {
        $front_page = file_get_contents('frontpage.md');
    } else {
        $front_page = '';
    }
    return view('welcome', compact('front_page'));
});

Route::Resource('page', PageController::class)
        ->only(['show']);

require __DIR__.'/auth.php';

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/info', function() {
    return phpinfo();
});

Route::get('/usertest', function() {
    return view('usertest');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
});

Route::get('/meshphone/search', [MeshPhoneController::class, 'search'])
    ->name('meshphone.search');
Route::resource('meshphone', MeshPhoneController::class)
    ->only(['index', 'create', 'store', 'edit', 'update', 'destroy'])
    ->middleware(['auth']);
Route::get('/meshphone/{id}/delete', [MeshPhoneController::class, 'destroy'])
    ->name('meshphone.delete')
    ->middleware(['auth']);
Route::Resource('meshphone', MeshPhoneController::class)
        ->only(['show']);

Route::get('/network_json', function () {
    return redirect('/api/network_json');
});
Route::get('/network_xml_v2', function () {
    return redirect('/api/network_xml_v2');
});
Route::get('/network_xml_v1', function () {
    return redirect('/api/network_xml_v1');
});
