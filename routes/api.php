<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\CheckinController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/network_json', [ExportController::class, 'network_json'])
    ->name('network.json');
Route::get('/network_xml_v1', [ExportController::class, 'network_xml_v1'])
    ->name('network.xml_v1');
Route::get('/network_xml_v2', [ExportController::class, 'network_xml_v2'])
    ->name('network.xml_v2');

Route::post('/checkin', [CheckinController::class, 'checkin'])
    ->name('api.checkin');
